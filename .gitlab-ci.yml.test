image: ubuntu:latest

stages:
  - build-test
  - deploy

variables:
  RUST_VERSION: "stable"
  ZIG_VERSION: "0.10.0"
  CARGO_LAMBDA_VERSION: "v1.0.3"
 

build-test:
  before_script:
    - apt-get update -qy
    - apt-get install -y curl jq wget xz-utils build-essential 
  stage: build-test
  script:
    - curl https://sh.rustup.rs -sSf | sh -s -- -y --default-toolchain $RUST_VERSION
    - wget https://ziglang.org/download/${ZIG_VERSION}/zig-linux-x86_64-${ZIG_VERSION}.tar.xz
    - tar -xf zig-linux-x86_64-${ZIG_VERSION}.tar.xz  
    - mv zig-linux-x86_64-${ZIG_VERSION}/* /usr/local/bin
    - wget https://github.com/cargo-lambda/cargo-lambda/releases/download/$CARGO_LAMBDA_VERSION/cargo-lambda-$CARGO_LAMBDA_VERSION.x86_64-unknown-linux-musl.tar.gz -O /tmp/cargo-lambda.tar.gz
    - tar -xzvf /tmp/cargo-lambda.tar.gz -C /tmp
    - mv /tmp/cargo-lambda ~/.cargo/bin/cargo-lambda

    - export PATH="$HOME/.cargo/bin:$PATH" 
    - rustc --version
    - zig version
    - cargo lambda --version 
    - make lint 
    - make format 
    - make test 
    - make build 

deploy:
  stage: deploy  
  services:
    - docker:20.10.16-dind
  needs: ["build-test"]  
  script:
    - aws ecr get-login-password --region us-east-1 | docker login --username AWS --password-stdin $EC
    - docker build -t week7 .
    - docker tag week7:latest $ECR
    - docker push $ECR